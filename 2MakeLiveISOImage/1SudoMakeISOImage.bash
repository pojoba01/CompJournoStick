#! /bin/bash
#
# Copyright (C) 2013 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

# figure out architecture
ARCH=\
`yum version nogroups|grep Installed|sed 's;^.*/;;'|sed 's; .*$;;'`
echo "Fedora installed architecture is ${ARCH}"

./sudo-make-iso.bash $ARCH
