%end

# Load stuff into the image
%post --nochroot

# DNS for R package installs
mkdir -p $INSTALL_ROOT/etc
cp /etc/resolv.conf $INSTALL_ROOT/etc/resolv.conf

# copy the project files
mkdir -p $INSTALL_ROOT/opt
rm -fr $INSTALL_ROOT/opt/CompJournoStick
cp -a /opt/Project $INSTALL_ROOT/opt/CompJournoStick
chown -R 1000:1000 $INSTALL_ROOT/opt/CompJournoStick
%end

%post
pushd /opt/CompJournoStick/1SetUpWorkstation/

# do the source installs, etc.
export PATH=$PATH:/usr/sbin:/usr/local/sbin:/usr/local/bin:/sbin:/root/bin
sudo Maint/sudo-after-yum.bash
wget http://fedora.vitu.ch/QGIS/qgis.repo -P /etc/yum.repos.d/

popd
%end
