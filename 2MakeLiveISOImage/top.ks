# CompJournoStick version maintained by M. Edward (Ed) Borasky
# http://znmeb.github.io
# mailto:znmeb@znmeb.net

%include fedora-live-desktop.ks
part / --size 16384 --fstype ext4
#repo --name=updates-testing --mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=updates-testing-f$releasever&arch=$basearch
repo --name=RStudioRepo --baseurl=file:///opt/RStudioRepo # RStudio RPMs live here
repo --name=QGISRepo --baseurl=http://fedora.vitu.ch/QGIS/$releasever/$basearch

%packages
