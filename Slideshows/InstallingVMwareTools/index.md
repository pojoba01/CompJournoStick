Installing VMware Tools
=======================
author: M. Edward (Ed) Borasky
date: September 13, 2014
autosize: true

Start the Virtual Machine and Log In
====================================
<img height=450 src="http://i.imgur.com/rmIUku1.png">
 
In the 'Virtual Machine' menu, select "Reinstall VMware Tools"

VMware Tools Dialog Box
=======================
<img height=450 src="http://i.imgur.com/inUM6xy.png">

Press 'Install'

VMware Tools Alert at Bottom of Screen
======================================
<img height=450 src="http://i.imgur.com/QVdaWKA.png">

Close this with the 'x'

Open a Terminal Window
======================
<img height=450 src="http://i.imgur.com/SAWMnxW.png">

Type `cd CompJourno<tab>1<tab><enter>`; `sudo ./2SudoVM<tab><enter>`; enter your password

VMware Tools Installation Complete
==================================
<img height=450 src="http://i.imgur.com/IWBc8Mr.png">

Reboot the virtual machine.

Next Step
=========
[Making an ISO File](http://znmeb.github.io/CompJournoStick/Slideshows/MakinganISOFile)

Bugs? Questions? Comments?
==========================
[Open an issue on Github](https://github.com/znmeb/CompJournoStick/issues/new)
<div>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">CompJournoStick Documentation</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://znmeb.github.io/CompJournoStick" property="cc:attributionName" rel="cc:attributionURL">M. Edward (Ed) Borasky</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</div>
<div>
<a title="Clicky Real Time Web Analytics" href="http://clicky.com/91521"><img alt="Clicky Real Time Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(91521); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/91521ns.gif" /></p></noscript>
</div>
