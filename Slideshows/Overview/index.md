CompJournoStick Overview
========================
author: M. Edward (Ed) Borasky
date: September 13, 2014
autosize: true

I Keep Six Honest Serving Men
=============================
I keep six honest serving-men  
(They taught me all I knew);  
Their names are What and Why and When  
And How and Where and Who.  

- Rudyard Kipling, from "The Elephant's Child" in _Just So Stories_  
<http://c2.com/cgi/wiki?SixHonestServingMen>

Who Is CompJournoStick For?
===========================
* Journalism students
    * high school, community college and beyond
* Independent journalists
    * researchers, reporters, editors, publishers

Why?
====
* Journalists today work in a world dominated by three trends:
    * A need for information security tools,
    * Large sets of complex data with stories waiting to be told, and
    * Real-time many-to-many communications platforms.

Need for Information Security Tools
===================================
* News organizations are increasingly targets of
    * 'Friendly' government surveillance
    * Malicious actors

Large Complex Datasets
======================
* Government data - world, national, regional, and local
* Business and financial data
* Political fundraising and vote tabulation data
* Environmental, weather and climate data
* Social network data
* And yes - traffic and sports data too

Real-time Many-to-Many Communications
=====================================
* Hundreds of millions of Facebook and Twitter accounts almost everywhere in the world
* Millions of active Twitter users
* Complex patterns of interactions around people, places and events
* Intricate and changing connection patterns between people
* ___People break and discuss the news in real time on Twitter___.

How? Open Source Tools to the Rescue!
=====================================
* ___CompJournoStick___ is a complete Linux workstation, plus

Secure Communications - Email
=============================
* Evolution / GNU Privacy Guard (GPG)
* Thunderbird Enigmail
* Claws Mail

Secure Instant Messaging / Internet Relay Chat
==============================================
* Pidgin OTR
* BitlBee OTR
* XChat OTR
* Irssi OTR

Private / Anonymous Browsing
============================
* Firefox
* Tor
* Vidalia

Digital Media Creation / Editing
================================
* [Fedora Design Suite](http://spins.fedoraproject.org/design/) packages
* LyX
* Pandoc
* Sigil
* Calibre

Advanced Data Collection and Management
=======================================
* Web scraping
* PDF data extraction / optical character recognition
* Twitter API access
* Relational databases: MySQL/MariaDB, PostgreSQL and SQLite
* NoSQL databases: CouchDB, MongoDB and Redis
* Solr and Lucene search technology

Exploring, Analyzing and Presenting Data - R and RStudio
========================================================
* Financial, economic and time series
* Geospatial / mapping
* Natural language, text data, machine learning
* Social networks and graphs
* Semantic web
* Reproducible research

Exploring, Analyzing and Presenting Data - Python
===========================================================
* IPython Notebook
* Natural Language Tool Kit
* NetworkX: High-productivity software for complex networks
* pandas: Python Data Analysis Library
* scikit-learn: Machine Learning in Python

Geospatial Processing
=====================
* PostGIS / pgRouting
* Quantum GIS
* GRASS Geographic Resources Analysis Support System
* Mapnik
* Mapserver
* SpatiaLite
* SAGA System for Automated Geoscientific Analyses

Linux Packages Included:
========================
* <https://github.com/znmeb/CompJournoStick/tree/master/Docs/Packages>

R Packages Included:
====================
* <https://github.com/znmeb/CompJournoStick/blob/master/Docs/R-packages.csv>

Why 100 Percent Open Source?
============================
* Open source software is ___flexible___.
* Open source software is ___robust___.
* Open source software is ___low cost___.

Flexibility - CompJournoStick Can Run
=====================================
* On a workstation, laptop or server
    * In an isolated virtual machine,
    * From a read-only live DVD, leaving the machine untouched,
    * From a persistent read-write live USB drive, leaving the machine untouched, or
    * Installed on 'bare metal' for peak efficiency.

Robustness
==========
* Proven technologies in wide use
* Tools crafted by highly-motivated, self-regulated communities of experts
* Security flaws, functionality defects and performance issues rapidly found and fixed
* Peer review often yields software more efficient than commercial counterparts

Low Cost
========
* Freely downloadable with few legal restrictions
* Wide range of documentation and training material available on the World-Wide Web
* ___Functionality costing thousands of dollars per year in commercial licenses is available for the cost of a download!___

CompJournoStick Licenses
========================
* Code: [Affero GNU Public License, version 3](http://www.gnu.org/licenses/agpl-3.0.html)
* Documentation: [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](http://creativecommons.org/licenses/by-sa/4.0/)
* [Some export restrictions apply!](https://fedoraproject.org/wiki/Legal:Export)

When and Where
==============
* 3.0.0 Release: September 13, 2014
* On Github at <http://znmeb.github.io/CompJournoStick>

The Rest of the Story
=====================
_(Did you know there was more?)_
 
I keep six honest serving-men  
(They taught me all I knew);  
Their names are What and Why and When  
And How and Where and Who.  
I send them over land and sea,  
I send them east and west;  
But after they have worked for me,  
_I_ give them all a rest.  

I Keep Six Honest Serving Men (continued)
=========================================
_I_ let them rest from nine till five,  
For I am busy then,  
As well as breakfast, lunch, and tea,  
For they are hungry men;  
But different folk have different views:  
I know a person small -  
She keeps ten million serving-men,  
Who get no rest at all!  
She sends 'em abroad on her own affairs,  
From the second she opens her eyes -  
One million Hows, two million Wheres,  
And seven million Whys!

Information Security Resources
==============================
* [Information Security - Reports - Committee to Protect Journalists](http://cpj.org/reports/2012/04/information-security.php)
* [Jonathan Stray: Lecture 8: Security, Surveillance, and Privacy | JMSC6041 Computational Journalism](http://courses.jmsc.hku.hk/jmsc6041spring2013/2013/02/13/lecture-8-security-surveillance-and-privacy/)
* [The Amnesiac Incognito Live System (Tails)](https://tails.boum.org/)
* [Whonix - Anonymous Operating System](https://www.whonix.org/wiki/Main_Page)
* [Fedora Linux - Creating GNU Privacy Guard (GPG) Keys](https://fedoraproject.org/wiki/Creating_GPG_Keys)
* [Fedora Linux - Using GPG With Evolution](https://fedoraproject.org/wiki/Using_GPG_with_Evolution)

Desktop Resources
=================
* [Fedora Linux Installation Guide](http://docs.fedoraproject.org/en-US/Fedora/20/html/Installation_Guide/index.html)
* [GNOME 3 Desktop Users' Documentation](https://help.gnome.org/users/)
* [LibreOffice Documentation](https://www.libreoffice.org/get-help/documentation/)
* [Evolution Email and Calendar](https://help.gnome.org/users/evolution/stable/)

R / RStudio Resources
=====================
* [R for Journalists](http://www.scoop.it/t/r-for-journalists)
* [Sharon Machlis: Beginner's Guide to R](https://www.computerworld.com/s/article/9239625/Beginner_s_guide_to_R_Introduction)
* [rjournos Google group](https://groups.google.com/forum/#!forum/rjournos)
* [R Online Manuals](http://cran.rstudio.com/manuals.html)
* [R Commander Home Page](http://socserv.mcmaster.ca/jfox/Misc/Rcmdr/)
* [RStudio Documentation](https://support.rstudio.com/hc/en-us/categories/200035113-Documentation)


Next Step
=========
[Creating a Virtual Machine](http://znmeb.github.io/CompJournoStick/Slideshows/CreatingaVirtualMachine)

Bugs? Questions? Comments?
==========================
[Open an issue on Github](https://github.com/znmeb/CompJournoStick/issues/new)
<div>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">CompJournoStick Documentation</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://znmeb.github.io/CompJournoStick" property="cc:attributionName" rel="cc:attributionURL">M. Edward (Ed) Borasky</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</div>
<div>
<a title="Clicky Real Time Web Analytics" href="http://clicky.com/91521"><img alt="Clicky Real Time Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(91521); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/91521ns.gif" /></p></noscript>
</div>
