Making an ISO File
==================
author: M. Edward (Ed) Borasky
date: September 13, 2014
autosize: true

What is an ISO File?
====================
* A disk file image of a CD/DVD
* Can be booted in a virtual machine
* Can be burned to a bootable DVD
* Can be installed to a bootable persistent USB stick

Start VM, Log In, Open a Terminal
=================================
<img height=450 src="http://i.imgur.com/8dPBpp4.png">

Type `cd CompJourno<tab>2<tab><enter>`

ISO File Options
================
1. The same architecture as you're using
    * `sudo ./1<tab><enter>`
2. A 32-bit ISO
    * `sudo ./sudo-32<tab><enter>`
3. A 64-bit ISO
    * `sudo ./sudo-64<tab><enter>`
4. Both 32-bit and 64-bit ISOs
    * `sudo ./sudo-both<tab><enter>`

Enter your password; wait

This Will Take A Long Time
==========================
1. For each architecture (32-bit, 64-bit or both):
    * Download all the Linux packages.
    * Install them to an emulated Linux filesystem.
    * Download and install the R packages to the emulated system.
    * Create a compressed, bootable ISO image of the emulated system.
2. When it's done, if all went well, you will have ISO image(s):
   * /opt/ISOs/CompJournoStick20-i686.iso (32-bit)
   * /opt/ISOs/CompJournoStick20-x86_64.iso (64-bit)

What A Successful Run Looks Like
================================
<img height=450 src="http://i.imgur.com/V7NTVU2.png">

If It Didn't Work
=================
- File an issue at <https://github.com/znmeb/CompJournoStick/issues/new>
- Include the log file at `/opt/CompJournoStick/*log`

Next Step
=========
[Creating a Persistent Live USB Stick](http://znmeb.github.io/CompJournoStick/Slideshows/CreatingaPersistentLiveUSBStick)

Bugs? Questions? Comments?
==========================
[Open an issue on Github](https://github.com/znmeb/CompJournoStick/issues/new)
<div>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">CompJournoStick Documentation</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://znmeb.github.io/CompJournoStick" property="cc:attributionName" rel="cc:attributionURL">M. Edward (Ed) Borasky</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</div>
<div>
<a title="Clicky Real Time Web Analytics" href="http://clicky.com/91521"><img alt="Clicky Real Time Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(91521); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/91521ns.gif" /></p></noscript>
</div>
