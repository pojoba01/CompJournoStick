Installing the Linux and R Packages
===================================
author: M. Edward (Ed) Borasky
date: September 13, 2014
autosize: true

Prerequisites
=============
1. You'll need reliable wall power and reliable Internet bandwidth, preferably a wired connection.
1. You'll need a Fedora Linux system, installed as shown in [Installing Fedora](http://znmeb.github.io/CompJournoStick/Slideshows/InstallingFedora).
1. Updating the system strictly isn't necessary, but it's highly recommended. See [Updating Fedora](http://znmeb.github.io/CompJournoStick/Slideshows/UpdatingFedora).

BASH Completion
===============
The terminal emulator uses a shell called 'bash'. Bash features 'tab completion' - you type the first part of a command, file or directory name, type a TAB and bash completes the command or name! If there's more than one match, bash will list them.

For example, suppose you want to change directory (`cd`) to the 'CompJournoStick' directory. You only need to type

    cd Comp<tab><enter>

and you'll be there, assuming the directory exists. In the following, we'll use this heavily. &lt;tab&gt; denotes the TAB key and &lt;enter&gt; denotes the ENTER key.

Log in, Open Terminal, Browse to Releases
=========================================
<img height=450 src="http://i.imgur.com/8sTEVFM.png">

Type `firefox http://j.mp/CompJournoStickReleases<enter>`

CompJournoStick Releases on Github
==================================
<img height=450 src="http://i.imgur.com/b30tWQ7.png">

Scroll down to most recent source archives and click 'tar.gz' file

Open With Archive Manager
=========================
<img height=450 src="http://i.imgur.com/rDfzMuQ.png">

Press 'OK'

Archive Manager
===============
<img height=450 src="http://i.imgur.com/gFqPDOe.png">

Press 'Extract' at top

Archive Manager Dialog
======================
<img height=450 src="http://i.imgur.com/Ot63DKh.png">

Press 'Extract' at lower right

Extraction Complete
===================
<img height=450 src="http://i.imgur.com/KgKkog0.png">

Press 'Quit'

Go Back to Terminal Window
==========================
<img height=450 src="http://i.imgur.com/leeq8b0.png">

Type `cd CompJourno<tab>1<tab><enter>`; type `sudo ./1<tab><enter>`; enter your password

Wait
====
It will take some time to download and install the CompJournoStick packages. On a typical coffee shop WiFi and my dual-core laptop it takes about an hour to do all of the downloads and installs.

Date/Time/Time Zone Configuration
=================================
<img height=450 src="http://i.imgur.com/nN1HTNo.png">

Check the 'Synchronize date and time over the network' checkbox and then select the 'Time Zone' tab

Time Zone Tab
=============
<img height=450 src="http://i.imgur.com/xb7OWQZ.png">

Set your time zone and press 'OK'. You will need to reboot after installation!

Next Step
=========
[Installing VMware Tools](http://znmeb.github.io/CompJournoStick/Slideshows/InstallingVMwareTools)

Bugs? Questions? Comments?
==========================
[Open an issue on Github](https://github.com/znmeb/CompJournoStick/issues/new)
<div>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">CompJournoStick Documentation</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://znmeb.github.io/CompJournoStick" property="cc:attributionName" rel="cc:attributionURL">M. Edward (Ed) Borasky</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</div>
<div>
<a title="Clicky Real Time Web Analytics" href="http://clicky.com/91521"><img alt="Clicky Real Time Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(91521); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/91521ns.gif" /></p></noscript>
</div>
