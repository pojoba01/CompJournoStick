Updating Fedora
===============
author: M. Edward (Ed) Borasky
date: September 13, 2014
autosize: true

Encryption Passphrase Entry
===========================
<img height=450 src="http://i.imgur.com/WsRpeAn.png">


Enter the encryption passphrase you set during the install

GDM Display Manager
===================
<img height=450 src="http://i.imgur.com/TqC9uVw.png">

Press your user name to log in

Password Entry 
==============
<img height=450 src="http://i.imgur.com/iugEqQ8.png">

Enter your password and press 'Sign In'

GNOME 3 Welcome - Only on First Login
=====================================
<img height=450 src="http://i.imgur.com/q1XmXMF.png">

Press 'Next'

Input Sources
=============
<img height=450 src="http://i.imgur.com/D5GP5Gl.png">

Press 'Next'

Online Accounts
===============
<img height=450 src="http://i.imgur.com/AM01dq0.png">

Just press 'Next' - you can do this later!

Thank You
=========
<img height=450 src="http://i.imgur.com/H2WLr2K.png">

Press the big blue button to start using Fedora

GNOME Help
==========
<img height=450 src="http://i.imgur.com/RkCtMjs.png">

Close this with the 'x' at upper right

'Activities' Menu at Upper Left
===============================
<img height=450 src="http://i.imgur.com/B2N6o4Y.png">

Type 'term' in the search bar and press the 'Terminal' icon

Command Line Update
===================
<img height=450 src="http://i.imgur.com/rkVXhuL.png">

Type `sudo yum update -y<enter>`; enter your password. It will take a while to download and install the updates.

Complete!
=========
<img height=450 src="http://i.imgur.com/SwyeFRt.png">

Type `sudo reboot<enter>`. You may have to enter your password again.

Next Step
=========
[Installing the Linux and R Packages](http://znmeb.github.io/CompJournoStick/Slideshows/InstallingLinuxAndRPackages)

Bugs? Questions? Comments?
==========================
[Open an issue on Github](https://github.com/znmeb/CompJournoStick/issues/new)
<div>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">CompJournoStick Documentation</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://znmeb.github.io/CompJournoStick" property="cc:attributionName" rel="cc:attributionURL">M. Edward (Ed) Borasky</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</div>
<div>
<a title="Clicky Real Time Web Analytics" href="http://clicky.com/91521"><img alt="Clicky Real Time Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(91521); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/91521ns.gif" /></p></noscript>
</div>
