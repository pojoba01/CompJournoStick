Initializing the USB Stick
==========================
author: M. Edward (Ed) Borasky
date: September 13, 2014
autosize: true

Initializing the USB Stick
==========================
* The USB stick has all the Linux and R packages you need, but
    * 'root' and 'liveuser' have blank passwords - anyone can use the stick!
    * The time zone is not configured.

Boot the USB Stick on a Real Machine
====================================
<img height=450 src="http://i.imgur.com/YQuK2mY.png">

Select 'Try Fedora'.
.
Welcome to Fedora
=================
<img height=450 src="http://i.imgur.com/ulShUW5.png">

Press 'Close'.

Set 'root' and 'liveuser' Passwords in Terminal
===============================================
- Open a terminal.
- Type `sudo su -<enter>`.
    - You'll get a friendly warning.
- Type `passwd<enter>`.
- Enter a strong password.
- Type `exit<enter>`
- Type `passwd<enter>` again to set the 'liveuser' password.

Screenshot of Password Setting
==============================
<img height=450 src="http://i.imgur.com/ARwXSDz.png">

Pull Down the Upper Right Menu
==============================
<img height=450 src="http://i.imgur.com/FJgcv65.png">

Press the 'wrench' icon at the lower left to open 'Settings'.

'All Settings'
=================================
<img height=450 src="http://i.imgur.com/lJ9A0VD.png">

Select the 'Date & Time' entry at the lower left.

Date & Time
===========
<img height=450 src="http://i.imgur.com/yfWUGRn.png">

Turn on 'Automatic Time Zone'; close window with the 'x' at the upper right.

Bugs? Questions? Comments?
==========================
[Open an issue on Github](https://github.com/znmeb/CompJournoStick/issues/new)
<div>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">CompJournoStick Documentation</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://znmeb.github.io/CompJournoStick" property="cc:attributionName" rel="cc:attributionURL">M. Edward (Ed) Borasky</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</div>
<div>
<a title="Clicky Real Time Web Analytics" href="http://clicky.com/91521"><img alt="Clicky Real Time Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(91521); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/91521ns.gif" /></p></noscript>
</div>
