Installing Fedora
=================
author: M. Edward (Ed) Borasky
date: September 13, 2014
autosize: true

Download Fedora Desktop ISO File
================================
* [32-bit or 64-bit?](http://docs.fedoraproject.org/en-US/Fedora/20/html/Installation_Guide/ch-new-users.html#sn-which-arch)
    * 64-bit: <http://download.fedoraproject.org/pub/fedora/linux/releases/20/Live/x86_64/Fedora-Live-Desktop-x86_64-20-1.iso>
    * 32-bit: <http://download.fedoraproject.org/pub/fedora/linux/releases/20/Live/i386/Fedora-Live-Desktop-i686-20-1.iso>

Start VMware Player
===================
<img height=450 src="http://imgur.com/hYw3skD.png">

Edit virtual machine settings

Virtual Machine Settings
========================
<img height=450 src="http://imgur.com/LeXK5TA.png">

Select the CD/DVD settings

CD/DVD Settings
===============
<img height=450 src="http://imgur.com/YxaVjep.png">

Select 'Use ISO Image', press 'Browse'

Host-dependent File Browser
===========================
<img height=450 src="http://imgur.com/4TTkf19.png">

Open 'Fedora Live Desktop' ISO file you downloaded

Back at CD/DVD Settings
=======================
<img height=450 src="http://imgur.com/tS3byBd.png">

Press 'Save'

Back at Main VMware Player Window
=================================
<img height=450 src="http://imgur.com/xIkiBf4.png">

Press 'Play virtual machine' and wait for the boot to complete

Welcome to Fedora
=================
<img height=450 src="http://imgur.com/2YbjbFY.png">

Press 'Install to Hard Drive'

Select Language
===============
<img height=450 src="http://imgur.com/Zzo7zLF.png">

Select your language / keyboard and press 'Continue'


Installation Summary
====================
<img height=450 src="http://imgur.com/GKaPWIy.png">

First, press 'DATE & TIME'

Date and Time
=============
<img height=450 src="http://imgur.com/bYHnG8H.png">

Set your time zone and press 'Done'

Back at Installation Summary
============================
<img height=450 src="http://imgur.com/GKaPWIy.png">

Next, press 'NETWORK CONFIGURATION'

Network Configuration
=====================
<img height=450 src="http://imgur.com/pcxpA3X.png">

Set the 'Hostname' and press 'Done'

Back at Installation Summary
============================
<img height=450 src="http://imgur.com/GKaPWIy.png">

Next, press 'Installation Destination'

Installation Destination
========================
<img height=450 src="http://imgur.com/CC8cHE8.png">

Just press 'Done'

Installation Options
====================
<img height=450 src="http://i.imgur.com/iaJXGcA.png">

Check the 'Encrypt my data' box and press 'Continue'

Encryption Passphrase
=====================
<img height=450 src="http://i.imgur.com/wlcfpzK.png">

Enter a strong passphrase and press 'Save Passphrase'

Back at Installation Summary
============================
<img height=450 src="http://imgur.com/TfP0CO9.png">

Press 'Begin Installation'

Configuration
=============
<img height=450 src="http://imgur.com/EYzBOVR.png">

Press 'ROOT PASSWORD'

Root Password
=============
<img height=450 src="http://imgur.com/FXe73qY.png">

Enter a strong password twice and press 'Done'

Back at 'Configuration'
=======================
<img height=450 src="http://imgur.com/EYzBOVR.png">

Press 'USER CREATION'

Create User
===========
<img height=450 src="http://imgur.com/1X6ZPF3.png">

Fill in the form; check 'Make this user administrator'; press 'Done'; wait for installation to complete

Complete!
=========
<img height=450 src="http://imgur.com/W0A4AeR.png">

Press 'Quit'

Live System User Pull-down Menu
================================
<img height=450 src="http://imgur.com/LwY4nxS.png">

Select 'Power Off' ('power switch' icon at lower right)

Power Off
=========
<img height=450 src="http://imgur.com/clpiSfl.png">

Press 'Power Off'

Back at VMware Player Main Window
=================================
<img height=450 src="http://imgur.com/hYw3skD.png">

Press 'Edit virtual machine settings'

CD/DVD Settings
===============
<img height=450 src="http://imgur.com/Gsduqb0.png">

Select 'Use a physical drive' and press 'Save'

Next Step
=========
[Updating Fedora](http://znmeb.github.io/CompJournoStick/Slideshows/UpdatingFedora)

Bugs? Questions? Comments?
==========================
[Open an issue on Github](https://github.com/znmeb/CompJournoStick/issues/new)
<div>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">CompJournoStick Documentation</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://znmeb.github.io/CompJournoStick" property="cc:attributionName" rel="cc:attributionURL">M. Edward (Ed) Borasky</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</div>
<div>
<a title="Clicky Real Time Web Analytics" href="http://clicky.com/91521"><img alt="Clicky Real Time Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(91521); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/91521ns.gif" /></p></noscript>
</div>
