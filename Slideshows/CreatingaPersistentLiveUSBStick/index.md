Creating a Persistent Live USB Stick
====================================
author: M. Edward (Ed) Borasky
date: September 13, 2014
autosize: true

What Is a Persistent Live USB Stick?
====================================
* First, it's a USB stick.
* 'Live' means you can boot into the OS on a real or virtual machine from it.
* 'Persistent' means it remembers file changes between reboots.

What's So Cool About That?
==========================
* It doesn't touch the machine's hard drive unless you explicitly ask it to!
* You can boot it on one machine, put some data on it and share it with a colleague.
* You can lock it up in a desk drawer.

What You Will Need
==================
* A fresh USB stick of at least 8 GB
* USB 3.0 highly recommended
* A CompJournoStick virtual machine with a CompJournoStick ISO file on it

Open VMware Player, Check USB Settings
======================================
<img height=450 src="http://i.imgur.com/xgygzFU.png">

Press 'Save'; play virtual machine; log in

Plug in the USB Stick (USB 3.0 Preferred)
=========================================
<img height=450 src="http://i.imgur.com/meMnEy5.png">

Press 'OK'

Connect USB Stick to VM
=======================
* In 'Virtual Machine' menu
    * Select 'Removable Devices -> _name of USB stick_ -> Connect (Disconnect from Host)'

'Open With Files' Alert
=======================
<img height=450 src="http://i.imgur.com/X8ru6b7.png">

Press 'Open With Files'

Format the USB Stick
====================
<img height=450 src="http://i.imgur.com/u4etOvw.png">

Right-click on USB drive symbol and select 'Format...'

Format Volume
=============
<img height=450 src="http://i.imgur.com/pLCeTRl.png">

Press 'Format'. Don't bother with a name; a later step will over-write it.

Are you sure you want to format the volume?
===========================================
<img height=450 src="http://i.imgur.com/Y1miiOY.png">

Press 'Format'. When formatting is complete, close 'Files' window.

Start LiveUSB Creator in a Terminal
===================================
<img height=450 src="http://i.imgur.com/8fmTFcF.png">

Type `sudo liveusb-creator -m<enter>`; enter your password

LiveUSB Creator
===============
<img height=450 src="http://i.imgur.com/SZ9dXn8.png">

Press 'Browse' button.

Browse Dialog
=============
<img height=450 src="http://i.imgur.com/YOpwKyK.png">

Browse to `/opt/ISOs`; select ISO file; 'Open'

Slide 'Persistent Storage' to Far Right
=======================================
<img height=450 src="http://i.imgur.com/FPcl4dj.png">

Press the 'Create Live USB' button

'Complete!'
===========
<img height=450 src="http://i.imgur.com/5cteSTl.png">

Close LiveUSB Creator with 'x' at upper right

Next Step
=========
[Initializing the USB Stick](http://znmeb.github.io/CompJournoStick/Slideshows/InitializingTheUSBStick)

Bugs? Questions? Comments?
==========================
[Open an issue on Github](https://github.com/znmeb/CompJournoStick/issues/new)
<div>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">CompJournoStick Documentation</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://znmeb.github.io/CompJournoStick" property="cc:attributionName" rel="cc:attributionURL">M. Edward (Ed) Borasky</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</div>
<div>
<a title="Clicky Real Time Web Analytics" href="http://clicky.com/91521"><img alt="Clicky Real Time Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(91521); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/91521ns.gif" /></p></noscript>
</div>
