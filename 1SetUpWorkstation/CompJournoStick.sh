# CompJournoStick specific aliases and functions
export PATH=$PATH:$HOME/bin
alias l='ls -trCF'
alias ll='l -lA'
alias speed='sudo cpupower frequency-set -g performance'
alias battery='sudo cpupower frequency-set -g powersave'
alias auto='sudo powertop --auto-tune'
