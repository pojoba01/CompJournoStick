#! /bin/bash
#
# Copyright (C) 2013 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

grep -e 'not available' ${1} \
  | grep -v Tk \
  | grep -v installr \
  | grep -v ^package \
  | grep -v bigrquery \
  | grep -v rstudio \
  | grep -v manipulate \
  | grep -v ROpenOffice \
  | grep -v -e '‘’' \
  | grep -v -e "''"
grep "download of package .* failed" ${1}
