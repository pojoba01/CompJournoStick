#! /bin/bash
#
# Copyright (C) 2012 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

# import QGIS repository
wget -nc http://fedora.vitu.ch/QGIS/qgis.repo -P /etc/yum.repos.d/
rpm --import http://fedora.vitu.ch/Fedora/RPM-GPG-Key-vitu

# make and execute package installer
echo 'yum install -y --skip-broken \' > temp.bash
grep -v '\#' packagelist.txt | grep -v rstudio | grep -v '^$' \
  | sed 's/$/ \\/' >> temp.bash
bash -v temp.bash 2>&1 | tee temp.log
