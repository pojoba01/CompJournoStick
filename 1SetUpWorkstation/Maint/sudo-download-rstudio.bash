#! /bin/bash
#
# Copyright (C) 2013 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

export URL='http://www.rstudio.com/products/rstudio/download'

# make repo if it's not there already
mkdir -p /opt/RStudioRepo

# get the packages
pushd /opt/RStudioRepo
export RPM=`curl -sL ${URL}/${1}/|grep rpm|grep Fedora|sort -u|sed 's/^.*href="//'|sed 's/".*$//'`
wget --no-check-certificate -q -nc ${RPM}
popd

# (re)build the metadata
createrepo -v /opt/RStudioRepo

# add it to Yum if not there already
if [ ! -e /etc/yum.repos.d/opt_RStudioRepo.repo ]
then
  yum-config-manager --add-repo file:///opt/RStudioRepo
  echo 'gpgcheck=0' >> /etc/yum.repos.d/opt_RStudioRepo.repo
fi

# install/update RStudio
yum clean all
yum check-update
yum install -y rstudio
