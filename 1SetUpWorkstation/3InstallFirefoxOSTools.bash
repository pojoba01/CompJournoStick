#! /bin/bash
#
# Copyright (C) 2013 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

# Go to home directory
pushd ~

# install Firefox Nightly browser
echo 'Installing Firefox Nightly browser'
rm -fr Nightly
mkdir -p Nightly
cd Nightly
export WHERE='http://ftp.mozilla.org/pub/mozilla.org/firefox/nightly/latest-trunk'
export ARCH=`uname -m`
export WHAT=`curl ${WHERE}/|grep ${ARCH}|grep bz2|sed 's/^.*href="//'|sed 's/".*$//'`
wget ${WHERE}/${WHAT}
tar xf firefox*bz2

# Nightly won't start if base Firefox is running!
echo 'Close any open Firefox instances now!'
sleep 10
echo 'Optional: install Firefox OS simulators and ADB Helper'
sleep 10
~/Nightly/firefox/firefox https://ftp.mozilla.org/pub/mozilla.org/labs/fxos-simulator/
popd

# make a desktop file
mkdir -p ~/.local/share/applications
sed "s;HOME;${HOME};" nightly.desktop \
  > ~/.local/share/applications/nightly.desktop
