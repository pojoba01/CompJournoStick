#! /bin/bash
#
# Copyright (C) 2012 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

# make directory for package info files
export DOCPATH=../Docs/Packages
rm -fr $DOCPATH; mkdir -p $DOCPATH

# do 'yum info' for non-devel packages
for package in \
  `grep -v '\#' packagelist.txt \
    | grep -v '^$' \
    | grep -v '^\@' \
    | grep -v 'langpack' \
    | grep -v -e '-devel'`
do
  echo "Processing ${package}"
  yum info ${package} > $DOCPATH/${package}.yuminfo
done

# List the R packages
R --no-save --no-restore < R-scripts/list-R-packages.R

# Git
git add --all $DOCPATH
git add ../Docs/R-packages.csv
