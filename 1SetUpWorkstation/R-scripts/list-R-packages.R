#
# Copyright (C) 2012 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

p <- installed.packages(noCache=TRUE, fields=c('Title', 'URL'))
p <- subset(p, select=c(Package, Version, Title, URL))
p[,2] <- as.character(p[,2])
write.csv(p, file='../Docs/R-packages.csv', row.names=FALSE)
