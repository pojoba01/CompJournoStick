## CompJournoStick

___Computational Journalism on a Stick - a Fedora Remix for computational journalists___

CompJournoStick is derived from the [Computational Journalism Publishers Workbench](http://znmeb.github.io/Computational-Journalism-Publishers-Workbench/).

* [Get the latest release](https://github.com/znmeb/CompJournoStick/releases)
* [Open an issue on Github](https://github.com/znmeb/CompJournoStick/issues/new)
* [What you need to tell your lawyer](https://github.com/znmeb/CompJournoStick/blob/master/Docs/Legal.md)
* [Road map](https://github.com/znmeb/CompJournoStick/blob/master/Docs/RoadMap.md)

## Slideshows
1. [CompJournoStick Overview](http://znmeb.github.io/CompJournoStick/Slideshows/Overview)
1. [Creating a Virtual Machine](http://znmeb.github.io/CompJournoStick/Slideshows/CreatingaVirtualMachine)
1. [Installing Fedora](http://znmeb.github.io/CompJournoStick/Slideshows/InstallingFedora)
1. [Updating Fedora](http://znmeb.github.io/CompJournoStick/Slideshows/UpdatingFedora)
1. [Installing the Linux and R Packages](http://znmeb.github.io/CompJournoStick/Slideshows/InstallingLinuxAndRPackages)
1. [Installing VMware Tools](http://znmeb.github.io/CompJournoStick/Slideshows/InstallingVMwareTools)
1. [Making an ISO File](http://znmeb.github.io/CompJournoStick/Slideshows/MakinganISOFile)
1. [Creating a Persistent Live USB Stick](http://znmeb.github.io/CompJournoStick/Slideshows/CreatingaPersistentLiveUSBStick)
1. [Initializing the USB Stick](http://znmeb.github.io/CompJournoStick/Slideshows/InitializingTheUSBStick)

<div>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>
</div>
