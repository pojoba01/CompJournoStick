#! /bin/bash
#
# Copyright (C) 2013 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

export PATH=${PATH}:/usr/local/bin
export WHERE=http://lakshmi.calit2.uci.edu/census2000/censuspackages
for i in 1990blkgrp 2000 2000add 2000blkgrp 2000cdp 2000tract
do
  rm -fr UScensus*
  wget ${WHERE}/UScensus${i}_1.00.tar.gz
  tar xf UScensus${i}_1.00.tar.gz
  echo 'You will need to authenticate now'
  sudo R CMD INSTALL UScensus${i}/
done
