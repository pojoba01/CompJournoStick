#! /bin/bash
#
# Copyright (C) 2013 by M. Edward (Ed) Borasky
#
# This program is licensed to you under the terms of version 3 of the
# GNU Affero General Public License. This program is distributed WITHOUT
# ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THOSE OF NON-INFRINGEMENT,
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Please refer to the
# AGPL (http://www.gnu.org/licenses/agpl-3.0.txt) for more details.
#

export PATH=${PATH}:/usr/local/bin
export WHERE=http://lakshmi.calit2.uci.edu/census2000/R/src/contrib
for i in county cdp tract blkgrp
do
  rm -fr UScensus*
  wget ${WHERE}/UScensus2010${i}_1.00.tar.gz
  tar xf UScensus2010${i}_1.00.tar.gz
  echo 'You will need to authenticate now'
  sudo R CMD INSTALL UScensus2010${i}/
done
